import {
  Injectable,
  InternalServerErrorException,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import { UserRepository } from 'src/user/adapter/repository/users.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthCredentialsDto } from '../adapter/dto/auth-credentials.dto';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '../Entity/jwt-payload.interface';
import { User } from 'src/user/entity/user.entity';

@Injectable()
export class AuthService {
  private logger = new Logger('task service');
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  async signUp(useCredentials: AuthCredentialsDto): Promise<void> {
    return this.userRepository.createUser(useCredentials);
  }

  async signIn(
    useCredentials: AuthCredentialsDto,
  ): Promise<{ accessToken: string }> {
    const { email, password } = useCredentials;

    try {
      const user = await this.userRepository.findOneBy({ email: email });
      if (user && (await bcrypt.compare(password, user.password))) {
        const payload: JwtPayload = {
          email,
          username: '',
        };
        const accessToken = await this.jwtService.sign(payload);
        return { accessToken };
      } else {
        throw new UnauthorizedException('Invalid credentials');
      }
    } catch (error) {
      console.error(error);
      throw new InternalServerErrorException();
    }
  }

  async getAll(): Promise<User[]> {
    return this.userRepository.getAll();
  }
}
