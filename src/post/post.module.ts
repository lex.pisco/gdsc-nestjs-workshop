import { Module } from '@nestjs/common';
import { PostController } from './adapter/post.controller';
import { PostService } from './usecase/post.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostRepository } from './adapter/repository/posts.repository';
import { Posts } from './entity/post.entity';
import { JwtModule } from '@nestjs/jwt';
import { UserRepository } from 'src/user/adapter/repository/users.repository';
import { UserModule } from 'src/user/user.module';

@Module({
  imports: [
    JwtModule.register({
      secret: 'topSecret',
      signOptions: {
        expiresIn: 3600,
      },
    }),
    TypeOrmModule.forFeature([Posts]),
    UserModule,
  ],
  controllers: [PostController],
  providers: [PostService, PostRepository, UserRepository],
})
export class PostsModule {}
