import { Injectable, NotFoundException } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { Posts } from 'src/post/entity/post.entity';
import { CreatePostDto } from '../dto/create-post.dto';
import { User } from 'src/user/entity/user.entity';
import { UserRepository } from 'src/user/adapter/repository/users.repository';

@Injectable()
export class PostRepository extends Repository<Posts> {
  constructor(
    private dataSource: DataSource,
    private userRepository: UserRepository,
  ) {
    super(Posts, dataSource.createEntityManager());
  }

  async createPost(postDto: CreatePostDto, userId: User): Promise<Posts> {
    const { title, body } = postDto;

    const post = this.create({
      owner: userId,
      title,
      body,
    });
    return this.save(post);
  }

  async getPost(user: User): Promise<Posts[]> {
    const user1 = await this.userRepository.findOne({
      where: { id: user.id },
      relations: ['friends'],
    });
    const postOwnersId = [user.id, ...user1.friends.map((friend) => friend.id)];
    const query = this.createQueryBuilder('posts')
      .where('posts.ownerId IN (:...ids)', { ids: postOwnersId })
      .leftJoinAndSelect('posts.owner', 'owner');
    const allPosts = await query.getMany();
    return allPosts;
  }

  async removePost(postId: string, user: User): Promise<void> {
    const result = await this.delete({ id: postId, owner: user });

    if (result.affected === 0) {
      throw new NotFoundException();
    }
  }
}
