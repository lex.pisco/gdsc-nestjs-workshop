import { Injectable } from '@nestjs/common';
import { CommentRepository } from '../adapter/repository/comments.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Comment } from '../entity/comment.entity';
import { User } from 'src/user/entity/user.entity';

@Injectable()
export class CommentsService {
  constructor(
    @InjectRepository(CommentRepository)
    private commentRepository: CommentRepository,
  ) {}

  async addComment(
    context: string,
    userId: User,
    postId: string,
  ): Promise<Comment> {
    return await this.commentRepository.createComment(context, postId, userId);
  }

  async getAllCommentsFromPost(postId: string): Promise<Comment[]> {
    return this.commentRepository.getAllCommentFromPost(postId);
  }

  async getCommentsFromPost(
    postId: string,
    commentId: string,
  ): Promise<Comment> {
    return this.commentRepository.getCommentFromPost(postId, commentId);
  }

  async deleteComment(postId: string, commentId: string): Promise<void> {
    return this.commentRepository.deleteComment(postId, commentId);
  }
}
