import {
  Injectable,
  ConflictException,
  NotFoundException,
} from '@nestjs/common';
import { User } from 'src/user/entity/user.entity';
import { Comment } from 'src/comment/entity/comment.entity';
import { PostRepository } from 'src/post/adapter/repository/posts.repository';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class CommentRepository extends Repository<Comment> {
  constructor(
    private readonly dataSource: DataSource,
    private readonly postRepository: PostRepository,
  ) {
    super(Comment, dataSource.createEntityManager());
  }

  async createComment(
    context: string,
    postId: string,
    owner: User,
  ): Promise<Comment> {
    // Create Comment
    const post = await this.postRepository.findOneBy({ id: postId });
    const comment = await this.create({
      owner: owner,
      post: post,
      comment: context,
    });
    await this.save(comment);

    // Update Post
    if (!post) {
      throw new ConflictException('This post does not exist');
    }
    post.comment.push(comment);
    this.postRepository.save(post);

    return comment;
  }

  async getAllCommentFromPost(postId: string): Promise<Comment[]> {
    const post = await this.postRepository.findOneBy({ id: postId });
    const ids = post.comment.map((comment) => comment.id);
    if (ids.length < 1) {
      return [];
    }
    const query = this.createQueryBuilder('comment')
      .where('comment.id IN (:...ids)', {
        ids: ids,
      })
      .leftJoinAndSelect('comment.owner', 'owner');
    const allComments = await query.getMany();
    return allComments;
  }

  async getCommentFromPost(
    postId: string,
    commentId: string,
  ): Promise<Comment> {
    // const post = await this.postRepository.findOneBy({ id: postId });
    const comment = await this.findOneBy({ id: commentId });
    return comment;
  }

  async deleteComment(postId: string, commentId: string): Promise<void> {
    const post = await this.postRepository.findOneBy({ id: postId });
    const result = await this.delete({ id: commentId, post: post });

    if (result.affected === 0) {
      throw new NotFoundException();
    }
  }
}
