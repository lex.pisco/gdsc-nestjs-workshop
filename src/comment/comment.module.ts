import { Module } from '@nestjs/common';
import { CommentController } from './adapter/controller/comment.controller';
import { CommentsService } from './usecase/comment.service';
import { CommentRepository } from './adapter/repository/comments.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Comment } from './entity/comment.entity';
import { JwtModule } from '@nestjs/jwt';
import { PostRepository } from 'src/post/adapter/repository/posts.repository';
import { UserRepository } from 'src/user/adapter/repository/users.repository';
import { UserModule } from 'src/user/user.module';

@Module({
  imports: [
    JwtModule.register({
      secret: 'topSecret',
      signOptions: {
        expiresIn: 3600,
      },
    }),
    TypeOrmModule.forFeature([Comment]),
    UserModule,
  ],
  controllers: [CommentController],
  providers: [
    CommentsService,
    CommentRepository,
    PostRepository,
    UserRepository,
  ],
})
export class CommentsModule {}
