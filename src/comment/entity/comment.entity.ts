import { User } from 'src/user/entity/user.entity';
import { Posts } from 'src/post/entity/post.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Comment {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => User)
  @JoinColumn()
  owner: User;

  @ManyToOne(() => Posts)
  @JoinColumn()
  post: Posts;

  @Column()
  comment: string;
}
