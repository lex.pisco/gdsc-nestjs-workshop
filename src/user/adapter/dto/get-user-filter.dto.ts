export class GetUserFilterDto {
  q?: string;
  page?: number;
  limit?: number;
}
