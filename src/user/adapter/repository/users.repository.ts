import {
  Injectable,
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { DataSource, Like, Repository } from 'typeorm';
import { User } from 'src/user/entity/user.entity';
import { GetUserFilterDto } from '../dto/get-user-filter.dto';
import { AuthCredentialsDto } from 'src/auth/adapter/dto/auth-credentials.dto';
import * as bcrypt from 'bcrypt';
@Injectable()
export class UserRepository extends Repository<User> {
  constructor(private dataSource: DataSource) {
    super(User, dataSource.createEntityManager());
  }

  async createUser(authCredentialsDto: AuthCredentialsDto): Promise<void> {
    const { email, password } = authCredentialsDto;

    //hash
    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(password, salt);

    const user = this.create({ email, password: hashedPassword });

    try {
      await this.save(user);
    } catch (error) {
      if (error.code === '23505') {
        throw new ConflictException('Username Exists');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }
  async searchUser(filterDto: GetUserFilterDto): Promise<User[]> {
    const { q, page, limit } = filterDto;
    const skip = (page - 1) * limit;
    const users = await this.find({
      where: { email: Like(`%${q}%`) },
      skip,
      take: limit,
    });
    return users;
  }

  async getUser(id: string): Promise<User> {
    const user = await this.findOneBy({ id });
    return user;
  }

  async addUserAsFriend(
    currentUser: User,
    friendToAddId: string,
  ): Promise<User> {
    const user = currentUser;
    const friend = await this.findOneBy({ id: friendToAddId });

    if (!user || !friend) {
      throw new ConflictException(
        'Please make sure you are inputting the correct ID',
      );
    }
    try {
      const query = this.createQueryBuilder()
        .insert()
        .into('user_friends_user')
        .values([
          { userId_1: user.id, userId_2: friend.id },
          { userId_1: friend.id, userId_2: user.id },
        ])
        .execute();

      await query;
    } catch (error) {
      if (error.code === '23505') {
        throw new ConflictException("You're already friend with this person");
      }
    }

    return friend;
  }

  async deleteUserFromFriendList(
    currentUserId: string,
    friendId: string,
  ): Promise<User> {
    const user = await this.findOne({
      where: { id: currentUserId },
      relations: ['friends'],
    });

    const friend = await this.findOne({
      where: { id: friendId },
      relations: ['friends'],
    });

    if (!user || !friend) {
      throw new ConflictException(
        'Please make sure you are inputting the correct ID',
      );
    }

    user.friends = user.friends.filter((fr) => fr.id !== friendId);
    friend.friends = friend.friends.filter((fr) => fr.id !== currentUserId);
    this.save(friend);
    return this.save(user);
  }

  async getAllFriends(currentUserId: User): Promise<User[]> {
    const user = await this.findOne({
      where: { id: currentUserId.id },
      relations: ['friends'],
    });
    return user.friends;
  }

  async getAll(): Promise<User[]> {
    const query = this.createQueryBuilder('user');
    const user = await query.getMany();
    return user;
  }
}
