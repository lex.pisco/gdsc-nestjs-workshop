import {
  Controller,
  Get,
  Param,
  Post,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { UserService } from 'src/user/usecase/user.service';
import { AuthGuard } from '@nestjs/passport';
import { GetUserFilterDto } from '../dto/get-user-filter.dto';
import { User } from 'src/user/entity/user.entity';
import { GetUser } from 'src/user/usecase/get-user.decorator';

@Controller('')
@UseGuards(AuthGuard())
export class UsersController {
  constructor(private usersServive: UserService) {}

  @Get('users')
  async getUsers(@Query() filter: GetUserFilterDto): Promise<User[]> {
    return this.usersServive.getUser(filter);
  }
  @Get('users/:id')
  async getUserById(@Param('id') id: string): Promise<User> {
    return this.usersServive.getUserById(id);
  }

  @Post('me/friends/:id')
  async addFriend(
    @Param('id') userIdToAdd: string,
    @GetUser() user: User,
  ): Promise<User> {
    return this.usersServive.addFriend(user, userIdToAdd);
  }

  @Delete('me/friends/:id')
  async deleteFriend(
    @Param('id') friendId: string,
    @GetUser() user: User,
  ): Promise<User> {
    const { id } = user;
    return this.usersServive.deleteFriend(id, friendId);
  }

  @Get('me/friends')
  async getFriends(
    // @Param('id') userIdToAdd: string,
    @GetUser() user: User,
  ): Promise<User[]> {
    return this.usersServive.showFriends(user);
  }
}
