import { Module } from '@nestjs/common';
import { UserService } from './usecase/user.service';
import { UsersController } from './adapter/controller/user.controller';
import { UserRepository } from './adapter/repository/users.repository';
import { AuthModule } from 'src/auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entity/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User]), AuthModule],
  providers: [UserService, UserRepository],
  controllers: [UsersController],
})
export class UserModule {}
