import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GetUserFilterDto } from '../adapter/dto/get-user-filter.dto';
import { User } from '../entity/user.entity';
import { UserRepository } from '../adapter/repository/users.repository';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {}

  async getUser(filterDto: GetUserFilterDto): Promise<User[]> {
    return this.userRepository.searchUser(filterDto);
  }

  async getUserById(id: string): Promise<User> {
    return this.userRepository.getUser(id);
  }

  async addFriend(currentUserId: User, friendToAddId: string): Promise<User> {
    return this.userRepository.addUserAsFriend(currentUserId, friendToAddId);
  }

  async deleteFriend(currentUserId: string, friendId: string): Promise<User> {
    return this.userRepository.deleteUserFromFriendList(
      currentUserId,
      friendId,
    );
  }

  async showFriends(currentUserId: User): Promise<User[]> {
    return this.userRepository.getAllFriends(currentUserId);
  }
}
