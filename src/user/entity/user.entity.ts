import { Exclude } from 'class-transformer';
import { Posts } from 'src/post/entity/post.entity';
import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  email: string;

  @Column()
  name: string;

  @Column()
  @Exclude({ toPlainOnly: true })
  password: string;

  @ManyToMany(() => User, (user) => user.friends, { eager: false })
  @JoinTable()
  friends: User[];

  @OneToMany(() => Posts, (post) => post.owner, { eager: true, cascade: true })
  @JoinColumn()
  posts: Posts[];
}
