import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { PostsModule } from './post/post.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommentsModule } from './comment/comment.module';

@Module({
  imports: [
    UserModule,
    AuthModule,
    PostsModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'Lexoylouis!123',
      database: 'basic-social-media',
      autoLoadEntities: true,
      synchronize: true,
    }),
    CommentsModule,
  ],
})
export class AppModule {}
